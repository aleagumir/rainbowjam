﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class ScoreController : MonoBehaviour {
    float percentage = 0.0f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        percentage += 0.00125f;

        if (percentage > 1.0f)
            percentage = 0.0f;

        Slider scoreSlider = GetComponent<Slider>();
        scoreSlider.value = percentage;
	}
}
