﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LadderInputController : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            if(collision.GetComponent<PlayerInputController >() != null)
                collision.GetComponent<PlayerInputController>().onLadder = true;

            if (collision.GetComponent<EnemyController>() != null)
                collision.GetComponent<EnemyController>().onLadder = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            if (collision.GetComponent<PlayerInputController>() != null)
                collision.GetComponent<PlayerInputController>().onLadder = false;

            if (collision.GetComponent<EnemyController>() != null)
                collision.GetComponent<EnemyController>().onLadder = false;
        }
    }
}
