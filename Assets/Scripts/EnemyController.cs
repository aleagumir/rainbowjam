﻿using System.Collections;
using System.Linq;
using System.Collections.Generic;
using Spine.Unity;
using UnityEngine;

public class EnemyController : MonoBehaviour {

    public string idleAnimation = "";

    public string walkAnimation = "";

    public string interactAnimation = "";

    public float speedForce = 5f;

    public bool onLadder = false;

    private float cachedGravityScale = 0f;

    private string finalAnimation = "";

    private bool flipX = false;

    public List<WaypointData> path = new List<WaypointData>();

    public GameObject target;

    // Use this for initialization
    void Start () {
		cachedGravityScale = GetComponent<Rigidbody2D>().gravityScale;

        path = getPath(target.GetComponent<WaypointData>());
    }
	
	// Update is called once per frame
	void Update () {
 
        Rigidbody2D rigidBody2D = GetComponent<Rigidbody2D>();
        rigidBody2D.gravityScale = onLadder ? 0 : cachedGravityScale;

        WaypointData target = null;
        if (path.Count > 0)
        {
            target = path.First();

            float distance = Vector3.Magnitude(target.gameObject.transform.position - this.transform.position);

            if (distance < 1.5f)
            {
                path.Remove(target);

                if (path.Count > 0)
                {
                    target = path.First();
                }
            }
        }

        if (target != null)
        {
            Vector3 direction = target.gameObject.transform.position - this.transform.position;

            if(direction.x > 0.1f || direction.x < -0.1f)
            {
                flipX = direction.x < 0.0f;

                finalAnimation = walkAnimation;

                rigidBody2D.velocity = new Vector2(speedForce * (flipX ? -1.0f : 1.0f), rigidBody2D.velocity.y);
            }
            else
            {
                finalAnimation = idleAnimation;

                rigidBody2D.velocity = new Vector2(0f, rigidBody2D.velocity.y);
            }
        }
        else
        {
            finalAnimation = idleAnimation;

            rigidBody2D.velocity = new Vector2(0f, rigidBody2D.velocity.y);
        }

        if (onLadder)
        {
            Vector3 direction = target.gameObject.transform.position - this.transform.position;

            if (direction.y > 0.1f || direction.y < -0.1f)
            {
                rigidBody2D.velocity = new Vector2(rigidBody2D.velocity.x, speedForce * (direction.y > 0.0f ? 1.0f : -1.0f));
            }
            else
            {
                rigidBody2D.velocity = new Vector2(rigidBody2D.velocity.x, 0);
            }
        }

        SkeletonAnimation skeletonAnimation = GetComponent<SkeletonAnimation>();
        skeletonAnimation.AnimationName = finalAnimation;
        if(skeletonAnimation.skeleton != null)
            skeletonAnimation.skeleton.flipX = flipX;
    }

    List<WaypointData> getPath(WaypointData target)
    {
        path.Clear();

        if (target == null)
            return path;

        WaypointData[] waypoints = Object.FindObjectsOfType<WaypointData>();
        WaypointData closest = null;
        float closestDistance = 99999.0f;
        foreach (WaypointData waypoint in waypoints)
        {
            float distance = Vector3.Magnitude(waypoint.gameObject.transform.position - gameObject.transform.position);

            if ( distance < closestDistance)
            {
                closestDistance = distance;
                closest = waypoint;
            }
        }

        if (closest == null)
            return path;

        List<WaypointData> examined = new List<WaypointData>();

        examineNodesRecursive(path, examined, closest, target);

        return path;
    }

    bool examineNodesRecursive(List<WaypointData> path, List<WaypointData> examined, WaypointData source, WaypointData target)
    {
        path.Add(source);
        examined.Add(source);

        if (source == target)
            return true;

        foreach (GameObject neighbor in source.neighbours)
        {
            WaypointData neighborData = neighbor.GetComponent<WaypointData>();

            if (examined.Contains(neighborData))
                continue;

            if (examineNodesRecursive(path, examined, neighborData, target))
                return true;
        }

        path.Remove(source);

        return false;
    }
}
