﻿using System.Collections;
using System.Collections.Generic;
using Spine.Unity;
using UnityEngine;

public class PlayerInputController : MonoBehaviour {

    public string characterName = "";

    public Transform combinePoint;

    public GameObject partner;

    public PlayerController controller = PlayerController.Count;

    public string idleAnimation = "";

    public string walkAnimation = "";

    public string interactAnimation = "";

    public string combineAnimation = "";

    public string combinedInteractAnimation = "";

    private string activeInteractAnimation = "";

    public float speedForce = 5f;

    public bool onLadder = false;

    public bool onObject = false;

    public bool onObjectCombined = false;

    private float cachedGravityScale = 0f;

    private string finalAnimation = "";

    private bool flipX = false;

    private SkeletonAnimation skeletonAnimation;

    public enum PlayerController
    {
        Player1,
        Player2,
        Count
    };

    private enum KeyInput
    {
        KeyUp,
        KeyDown,
        KeyLeft,
        KeyRight,
        KeyInteractLeft,
        KeyInteractRight,
        Count
    };

    private readonly Dictionary<PlayerController, Dictionary<KeyInput, KeyCode>> keyMap = new Dictionary<PlayerController, Dictionary<KeyInput, KeyCode>>
    {
        {
            PlayerController.Player1,
            new Dictionary<KeyInput, KeyCode>
            {
                { KeyInput.KeyUp, KeyCode.W },
                { KeyInput.KeyDown, KeyCode.S },
                { KeyInput.KeyLeft, KeyCode.A },
                { KeyInput.KeyRight, KeyCode.D },
                { KeyInput.KeyInteractLeft, KeyCode.Q },
                { KeyInput.KeyInteractRight, KeyCode.E }
            }
        },
        {
            PlayerController.Player2,
            new Dictionary<KeyInput, KeyCode>
            {
                { KeyInput.KeyUp, KeyCode.I },
                { KeyInput.KeyDown, KeyCode.K },
                { KeyInput.KeyLeft, KeyCode.J },
                { KeyInput.KeyRight, KeyCode.L },
                { KeyInput.KeyInteractLeft, KeyCode.U },
                { KeyInput.KeyInteractRight, KeyCode.O }
            }
        }
    };

    // Use this for initialization
    void Start () {
		cachedGravityScale = GetComponent<Rigidbody2D>().gravityScale;

        skeletonAnimation = GetComponent<SkeletonAnimation>();
        skeletonAnimation.state.Complete += OnAnimationComplete;
    }
	
	// Update is called once per frame
	void Update () {

        Rigidbody2D rigidBody2D = GetComponent<Rigidbody2D>();
        float newGravityScale = cachedGravityScale;

        if (Input.GetKey(keyMap[controller][KeyInput.KeyLeft]))
        {
            finalAnimation = walkAnimation;

            flipX = true;

            rigidBody2D.velocity = new Vector2(-speedForce, rigidBody2D.velocity.y);
        }
        else if (Input.GetKey(keyMap[controller][KeyInput.KeyRight]))
        {
            finalAnimation = walkAnimation;

            flipX = false;

            rigidBody2D.velocity = new Vector2(speedForce, rigidBody2D.velocity.y);
        }
        else
        {
            finalAnimation = idleAnimation;

            rigidBody2D.velocity = new Vector2(0f, rigidBody2D.velocity.y);
        }

        if (onObject)
        {
            if (Input.GetKey(keyMap[controller][KeyInput.KeyInteractLeft]) || Input.GetKey(keyMap[controller][KeyInput.KeyInteractRight]))
            {
                activeInteractAnimation = interactAnimation;

                finalAnimation = activeInteractAnimation;
            }
        }
        else if(onObjectCombined)
        {
            if (Input.GetKey(keyMap[controller][KeyInput.KeyInteractLeft]) || Input.GetKey(keyMap[controller][KeyInput.KeyInteractRight]))
            {
                newGravityScale = 0.0f;

                if (activeInteractAnimation != combinedInteractAnimation)
                {
                    activeInteractAnimation = combineAnimation;

                    if(name == "Bug")
                    {
                        this.transform.position = Vector3.Lerp(this.transform.position, this.partner.GetComponent<PlayerInputController>().combinePoint.position, 0.125f);
                    }
                }
                else
                {
                    activeInteractAnimation = combinedInteractAnimation;
                }

                finalAnimation = activeInteractAnimation;
            }
            else
            {
                activeInteractAnimation = "";
            }
        }

        if (onLadder)
        {
            newGravityScale = 0.0f;

            if (Input.GetKey(keyMap[controller][KeyInput.KeyUp]))
            {
                rigidBody2D.velocity = new Vector2(rigidBody2D.velocity.x, speedForce);
            }
            else if (Input.GetKey(keyMap[controller][KeyInput.KeyDown]))
            {
                rigidBody2D.velocity = new Vector2(rigidBody2D.velocity.x, -speedForce);
            }
            else
            {
                rigidBody2D.velocity = new Vector2(rigidBody2D.velocity.x, 0);
            }
        }

        skeletonAnimation.AnimationName = finalAnimation;
        if(skeletonAnimation.skeleton != null)
            skeletonAnimation.skeleton.flipX = flipX;

        rigidBody2D.gravityScale = newGravityScale;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.collider.tag == "Player")
        {
            Physics2D.IgnoreCollision(this.gameObject.GetComponent<Collider2D>(), collision.collider, true);
        }
    }

    private void OnAnimationComplete(Spine.TrackEntry trackEntry)
    {
        if(trackEntry.animation.name == combineAnimation)
            activeInteractAnimation = combinedInteractAnimation;
    }
}
