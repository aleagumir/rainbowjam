﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectInputController : MonoBehaviour {

    public string activatedOnlyBy = "";

    private readonly Dictionary<string, int> playerRequirements = new Dictionary<string, int>
    {
        { "Bug", 1},
        { "Buddy", 1},
        { "Both", 2}
    };

    public List<PlayerInputController> players = new List<PlayerInputController>();

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        { 
            PlayerInputController pic = collision.GetComponent<PlayerInputController>();

            int requiredPlayers = playerRequirements[activatedOnlyBy];

            if (activatedOnlyBy == "Both" || pic.name == activatedOnlyBy)
            {
                players.Add(pic);

                if(players.Count == requiredPlayers)
                {
                    foreach(PlayerInputController player in players)
                    {
                        if (activatedOnlyBy == "Both")
                            player.onObjectCombined = true;
                        else
                            player.onObject = true;
                    }
                }                    
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            PlayerInputController pic = collision.GetComponent<PlayerInputController>();

            int requiredPlayers = playerRequirements[activatedOnlyBy];

            if (activatedOnlyBy == "Both" || pic.name == activatedOnlyBy)
            {
                if (players.Count == requiredPlayers)
                {
                    foreach (PlayerInputController player in players)
                    {
                        if (activatedOnlyBy == "Both")
                            player.onObjectCombined = false;
                        else
                            player.onObject = false;
                    }
                }

                players.Remove(pic);
            }
        }
    }
}
